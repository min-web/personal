import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@checkinfun/shared.module';

import { SampleComponent } from './sample.component';
import {MatIconModule, MatInputModule} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FlexLayoutModule} from "@angular/flex-layout";
import {DataTableModule} from "../../components/data-table/data-table.module";

const routes = [
    {
        path     : 'sample',
        component: SampleComponent
    }
];

@NgModule({
    declarations: [
        SampleComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        DataTableModule,
        MatIconModule,
        MatInputModule,
        BrowserAnimationsModule,
        FlexLayoutModule

    ],
    exports     : [
        SampleComponent
    ]
})

export class SampleModule
{
}
