
import {Component, OnInit} from '@angular/core';

import { FuseTranslationLoaderService } from '@checkinfun/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as turkish } from './i18n/tr';
import {fuseAnimations} from "@checkinfun/animations";
import {DataTableService} from "../../components/data-table/data-table.service";

@Component({
    selector   : 'sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss'],
    animations : fuseAnimations
})

export class SampleComponent implements OnInit
{
    /**
     * Constructor
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _tableService: DataTableService,
    )
    {
        this._fuseTranslationLoaderService.loadTranslations(english, turkish);
    }

    ngOnInit(): void {
        this._tableService.addButton(['create','delete']);
        this._tableService.setDisplayedColumns(['id', 'name','image','price']);
        this._tableService.setDataRow([
            {id: 1, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 2, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 3, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 4, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 1, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 1, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 1, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 1, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 1, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 1, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
            {id: 1, name: 'Hydrogen dasd asd asd asd ad ad a',image:{alt: 'Hinh 1',src:'assets/imgs/CheckinFun.png'},price:10000},
        ])
    }
}
