import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@checkinfun/fuse.module';
import { FuseSharedModule } from '@checkinfun/shared.module';
import { FuseSidebarModule } from '@checkinfun/components';

import { fuseConfig } from '../app/checkinfun-config';

import { AppComponent } from '../app/app.component';
import { LayoutModule } from '@layout/layout.module';
import { SampleModule } from '@main/sample/sample.module';
import { DataTableModule } from './components/data-table/data-table.module';
import { AuthModule } from './modules/auth/auth.module';
import { PagesModule } from '@modules/pages/pages.module';

const appRoutes: Routes = [];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseSharedModule,
        FuseSidebarModule,

        // App modules
        LayoutModule,
        SampleModule,
        AuthModule,
        LayoutModule,
        DataTableModule,
        PagesModule
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}