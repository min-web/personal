import { FormlyFieldConfig } from '@ngx-formly/core';
const regex = (regexCode, message) => {
    return {
      expression: c => regexCode.test(c.value),
      message: message
    };
  };
  
  const required = () => {
    return {
      expression: c => c.value,
      message: ''
    };
  };
  
  export const validation = {
    required: required(),

    email: regex(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,10}))$/,
      'email'
    ),
  
    phone: regex(
      /^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/,
      'contact number'
    ),
  
    password: regex(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/, 'password'),
  };