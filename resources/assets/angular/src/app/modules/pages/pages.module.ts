import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from '@modules/pages/pages.component';
import { HomeComponent } from '@modules/pages/home/home.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    PagesComponent,
    HomeComponent
  ],
  declarations: [PagesComponent, HomeComponent]
})
export class PagesModule { }
