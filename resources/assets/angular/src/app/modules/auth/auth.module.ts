import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormlyCommonModule } from '@common/formly/formly.module';
import { MaterialCommonModule } from '@common/material/material.module';
import { AuthRouter } from './auth.routing';
import { AuthComponent } from './auth.component';
import { TranslateModule } from '@ngx-translate/core';

import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { FuseSharedModule } from '@checkinfun/shared.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  imports: [
    CommonModule,
    FormlyCommonModule,
    MaterialCommonModule,
    AuthRouter,
    TranslateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,

    FuseSharedModule
  ],
  exports: [AuthComponent],
  declarations: [AuthComponent, RegisterComponent, LoginComponent]
})
export class AuthModule { }