import {Injectable, OnInit} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DataTableService {
    private __dataArrays: any[] = [];
    private __displayedColumns: any[] = [];
    private __btnActions:any[] =[];
    //----------------------------------

    public setDataRow(dataArrays: any[]) {

        this.__dataArrays = dataArrays;
    }
    public getDataRow() {
        return this.__dataArrays;
    }

    public setDisplayedColumns(displayedColumns:any[]) {
        if (this.__btnActions.length > 0) {
            this.__displayedColumns = displayedColumns.concat('actions');
        }else {
            this.__displayedColumns = displayedColumns
        }
    }

    public getDisplayedColumns() {
        return this.__displayedColumns;
    }

    public addButton(btnActions:any[]) {
        if (btnActions.length > 0) {
            this.__btnActions.concat(btnActions);
        }
    }

    public getButtonActions() {
        return this.__btnActions;
    }

    constructor() {}
}
