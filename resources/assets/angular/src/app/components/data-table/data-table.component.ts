import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {DataTableDataSource} from './data-table-datasource';
import {fuseAnimations} from "@checkinfun/animations";
import {DataTableService} from "./data-table.service";

@Component({
    selector: 'data-table',
    templateUrl: './data-table.component.html',
    styleUrls: ['./data-table.component.scss'],
    animations: fuseAnimations
})
export class DataTableComponent implements OnInit {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    dataSource: DataTableDataSource;
    displayedColumns: any[] = [];
    btnActions: any[] = [];

    constructor(public tableService: DataTableService) {

    }

    ngOnInit() {
        this.btnActions = this.tableService.getButtonActions();
        this.displayedColumns = this.tableService.getDisplayedColumns();
        this.dataSource = new DataTableDataSource(this.paginator, this.sort, this.tableService.getDataRow());
    }
}
