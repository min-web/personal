import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    MatRippleModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule
} from "@angular/material";
import {DataTableComponent} from "./data-table.component";
import {CdkTableModule} from "@angular/cdk/table";

@NgModule({
    imports: [
        CommonModule,
        CdkTableModule,
        MatButtonModule,
        MatChipsModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSortModule,
        MatTableModule,
        MatIconModule,
        MatMenuModule
    ],
    exports: [
        DataTableComponent,
        CdkTableModule,
        MatButtonModule,
        MatChipsModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSortModule,
        MatTableModule,
        MatIconModule,
        MatMenuModule
    ],
    declarations: [
        DataTableComponent
    ]
})
export class DataTableModule {
}
