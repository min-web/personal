import { Component, OnDestroy } from '@angular/core';
import { ConfirmService } from './confirm.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnDestroy {

  constructor(public _confirm: ConfirmService) { }

  ngOnDestroy() {
    this._confirm.title = 'Confirm Your Action';
  }
}
