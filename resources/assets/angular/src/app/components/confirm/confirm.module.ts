import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmComponent } from './confirm.component';
import { MaterialCommonModule } from '@common/material/material.module';

@NgModule({
  imports: [CommonModule, MaterialCommonModule],
  exports: [ConfirmComponent],
  declarations: [ConfirmComponent],
  entryComponents: [ConfirmComponent]
})
export class ConfirmModule {}
