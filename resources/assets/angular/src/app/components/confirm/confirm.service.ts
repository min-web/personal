import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfirmService {
  public title: string;
  constructor() {
    this.title = 'Confirm Your Action';
  }
}
