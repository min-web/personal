<!DOCTYPE HTML>
<html>
<head>
<base href="/">
<title>Preface a Personal Category Flat Bootstrap Responsive Website Template| Home :: w3layouts</title>
<link href="assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<script src="assets/js/jquery.min.js"></script>
<link href="assets/css/style.css" rel='stylesheet' type='text/css' />
 <!-- Custom Theme files -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="keywords" content="Preface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<!-- webfonts -->
<link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<!-- webfonts -->
 <!---- start-smoth-scrolling---->
<script type="text/javascript" src="assets/js/move-top.js"></script>
<script type="text/javascript" src="assets/js/easing.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
</head>
<body>	

<app></app>

<link href="assets/css/owl.carousel.css" rel="stylesheet">
<script src="assets/js/owl.carousel.js"></script>
<script>
	$(document).ready(function () {
		$("#owl-demo1").owlCarousel({
			items: 1,
			lazyLoad: false,
			autoPlay: true,
			navigation: false,
			navigationText: false,
			pagination: true,
		});
	});

</script>
<!-- Feedback -->
<script>
	$(document).ready(function () {
		$("#owl-demo3").owlCarousel({
			items: 1,
			lazyLoad: false,
			autoPlay: true,
			navigation: false,
			navigationText: true,
			pagination: true,
		});
	});

</script>
				
<script src="assets/js/easyResponsiveTabs.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#horizontalTab').easyResponsiveTabs({
			type: 'default',         
			width: 'auto',
			fit: true
		});
	});

</script>
					
<script type="text/javascript">
	$(document).ready(function () {
		$().UItoTop({
			easingType: 'easeOutQuart'
		});

	});
</script>
<script src="assets/js/bootstrap.js"></script>

<script type="text/javascript" src="js/app/runtime.js"></script>
<script type="text/javascript" src="js/app/polyfills.js"></script>
<script type="text/javascript" src="js/app/styles.js"></script>
<script type="text/javascript" src="js/app/vendor.js"></script>
<script type="text/javascript" src="js/app/main.js"></script>
</body>
</html>